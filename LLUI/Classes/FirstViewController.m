//
//  FirstViewController.m
//  testUtil
//
//  Created by zhouwei on 2018/4/14.
//  Copyright © 2018年 zhouwei. All rights reserved.
//

#import "FirstViewController.h"
#import "SecViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
- (IBAction)click:(id)sender {
    [self.navigationController pushViewController:[SecViewController new] animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
