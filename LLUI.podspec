#
# Be sure to run `pod lib lint LLUI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'LLUI'
  s.version          = '0.1.8'
  s.summary          = 'A short description of LLUI.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
  description of the pod here.
                       DESC

  s.homepage         = 'https://sky_zhou129@bitbucket.org/sky_zhou129/llui'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'zhouwei' => 'zhouyikai129@163.com' }
  s.source           = { :git => 'https://sky_zhou129@bitbucket.org/sky_zhou129/llui.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  #s.source_files = 'LLUI/Classes/**/*'
  
  s.source_files  = "LLUI/Classes/**/*.{h,m}"

  s.resource = "LLUI/Classes/**/*.{storyboard,xib,png,gif,xcassets}"

  # s.resource_bundles = {
  #   'LLUI' => ['LLUI/LUI/*']
  # }


  #s.resource_bundles = {
  # 'MyFramework' => ['Pod/Classes/**/*.{storyboard,xib}']
  #}

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
  s.dependency 'LLUitil', '~> 0.1.1'
end
